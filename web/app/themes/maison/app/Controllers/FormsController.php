<?php

namespace App\Controllers;

use App\Classes\PostTypes;
use Sober\Controller\Controller;

class FormsController extends Controller
{
    const HEADERS = [
        'content-type: text/html'
    ];

    const EMAIL = 'prikhodkin_ilya@mail.ru';

    public function callback_action()
    {
        self::send('partials.blocks.forms.consultation-tpl', $_POST, self::EMAIL, 'Заявка на консультацию', 'Заявка на консультацию');
    }

    public function subscribe_action()
    {
        self::send('partials.blocks.forms.subscribe-tpl', $_POST, self::EMAIL, 'Подписка на рассылку', 'Подписка на рассылку');
    }

    public function program_action()
    {
        self::send('partials.blocks.forms.program-tpl', $_POST, self::EMAIL, 'Интегральные программы', 'Интегральные программы');
    }

    public function quiz_action()
    {
        self::send('partials.blocks.forms.quiz-tpl', $_POST, self::EMAIL, 'Квиз', 'Квиз');
    }

    private function send($template, $query, $email, $title, $post_title)
    {

        $message = \App\template($template, compact('query'));
        $post_data = array(
            'post_type' => PostTypes::FORMS,
            'post_content' => $message,
            'post_status' => 'publish',
        );
        $post_id = wp_insert_post($post_data);
        wp_update_post(['ID' => $post_id, 'post_title' => $post_title.'#' . $post_id]);

        if (!empty($post_id)) {
            wp_mail($email, $title .' '. $post_id, $message, self::HEADERS);
            wp_send_json(['success' => true]);
        } else {
            wp_send_json(['success' => false]);
        }
    }
}
