<?php

namespace App\Controllers;


use Sober\Controller\Controller;

class TemplateServices extends Controller
{
    public function services() {
        $args = array(
            'post_type'      => 'services',
            'post_status'    => 'publish',
            'posts_per_page' => -1,
            'orderby' => 'date',
            'order' => 'ASC',
            'tax_query' => array(
                array(
                    'taxonomy' => 'services_type',
                    'field'    => 'slug',
                    'terms'    => 'programs'
                )
            )
        );

        return new \WP_Query($args);
    }

    public function other() {
        $args = array(
            'post_type'      => 'services',
            'post_status'    => 'publish',
            'posts_per_page' => -1,
            'order' => 'ASC',
            'tax_query' => array(
                array(
                    'taxonomy' => 'services_type',
                    'field'    => 'slug',
                    'terms'    => 'ostalnoe'
                )
            )
        );

        return new \WP_Query($args);
    }
}
