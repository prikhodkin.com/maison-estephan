<?php

namespace App\Controllers;


use Sober\Controller\Controller;

class SingleBlog extends Controller
{
    public function posts() {
        $args = array(
            'post_type'      => 'blog',
            'post_status'    => 'publish',
            'posts_per_page' => 3,
            'post__not_in' => array(get_the_ID())
        );

        return new \WP_Query($args);
    }
}
