<?php

namespace App\Controllers;


use Sober\Controller\Controller;

class ArchiveBlog extends Controller
{
    public function posts() {
        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

        $args = array(
            'post_type'      => 'blog',
            'post_status'    => 'publish',
            'posts_per_page' => 6,
            'paged' => $paged
        );

        return new \WP_Query($args);
    }
}
