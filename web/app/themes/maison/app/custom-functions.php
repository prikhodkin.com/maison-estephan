<?php
/*
 * Удаляет подключение стилей и скриптов от WP | Del WP Styles
 *
 */

function disable_wp_emojis_in_tinymce($plugins)
{
    if (is_array($plugins)) {
        return array_diff($plugins, array( 'wpemoji' ));
    } else {
        return array();
    }
}

function wpassist_remove_block_library_css()
{
    wp_dequeue_style('wp-block-library');
}

function remove_recent_comments_css()
{
    global $wp_widget_factory;
    remove_action('wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ));
}

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');
remove_action('wp_head', 'wp_oembed_add_discovery_links');
remove_action('wp_head', 'wp_oembed_add_host_js');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'rest_output_link_wp_head');
remove_action('wp_head', 'wp_resource_hints', 2);
remove_filter('the_content_feed', 'wp_staticize_emoji');
remove_filter('comment_text_rss', 'wp_staticize_emoji');
remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
add_filter('tiny_mce_plugins', 'disable_wp_emojis_in_tinymce');
add_action('widgets_init', 'remove_recent_comments_css');
add_action('wp_enqueue_scripts', 'wpassist_remove_block_library_css');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'wp_shortlink_wp_head');

/*
 * END Del WP Styles
 */

add_filter('upload_mimes', 'svg_upload_allow');

# Добавляет SVG в список разрешенных для загрузки файлов.
function svg_upload_allow($mimes)
{
    $mimes['svg']  = 'image/svg+xml';

    return $mimes;
}

add_filter('wp_check_filetype_and_ext', 'fix_svg_mime_type', 10, 5);

# Исправление MIME типа для SVG файлов.
function fix_svg_mime_type($data, $file, $filename, $mimes, $real_mime = '')
{

    // WP 5.1 +
    if (version_compare($GLOBALS['wp_version'], '5.1.0', '>=')) {
        $dosvg = in_array($real_mime, [ 'image/svg', 'image/svg+xml' ]);
    } else {
        $dosvg = ( '.svg' === strtolower(substr($filename, -4)) );
    }

    // mime тип был обнулен, поправим его
    // а также проверим право пользователя
    if ($dosvg) {
        // разрешим
        if (current_user_can('manage_options')) {
            $data['ext']  = 'svg';
            $data['type'] = 'image/svg+xml';
        }
        // запретим
        else {
            $data['ext'] = $type_and_ext['type'] = false;
        }
    }

    return $data;
}

add_filter('wp_prepare_attachment_for_js', 'show_svg_in_media_library');

# Формирует данные для отображения SVG как изображения в медиабиблиотеке.
function show_svg_in_media_library($response)
{
    if ($response['mime'] === 'image/svg+xml') {
        // С выводом названия файла
        $response['image'] = [
            'src' => $response['url'],
        ];
    }

    return $response;
}

/**
 * Удалить пустые элементы из массива
 * @param array $array
 * @param array $symbols удаляемые значения
 * @return array
 */
function array_delete(array $array, array $symbols = array(''))
{
    return array_diff($array, $symbols);
}

function get_current_slug()
{
    $url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $arr = array_delete(explode('/', $url));
    $arrReverse = array_reverse($arr);
    $slug = $arrReverse[0];
    return $slug;
}


function add_individual_style()
{
    $slug = get_current_slug();

    if (!is_front_page()) {
        wp_enqueue_style(
            $slug,
            get_template_directory_uri() . '/assets/dist/' . get_manifest_link("css/". $slug . ".min.css", get_template_directory_uri() . '/assets/dist/rev-manifest.json'),
            null,
            null
        );
    }
}

/*
 * "Хлебные крошки" для WordPress
 * автор: Dimox
 * версия: 2019.03.03
 * лицензия: MIT
*/
function dimox_breadcrumbs()
{

    /* === ОПЦИИ === */
    $text['home'] = 'Главная'; // текст ссылки "Главная"
    $text['category'] = '%s'; // текст для страницы рубрики
    $text['search'] = 'Результаты поиска по запросу "%s"'; // текст для страницы с результатами поиска
    $text['tag'] = 'Записи с тегом "%s"'; // текст для страницы тега
    $text['author'] = 'Статьи автора %s'; // текст для страницы автора
    $text['404'] = 'Ошибка 404'; // текст для страницы 404
    $text['page'] = 'Страница %s'; // текст 'Страница N'
    $text['cpage'] = 'Страница комментариев %s'; // текст 'Страница комментариев N'

    $wrap_before = '<ul class="breadcrumbs__list" itemscope itemtype="https://schema.org/BreadcrumbList">'; // открывающий тег обертки
    $wrap_after = '</ul>'; // закрывающий тег обертки
    $sep = ''; // разделитель между "крошками"
    $before = '<li class="breadcrumbs__item breadcrumbs__item--current">'; // тег перед текущей "крошкой"
    $after = '</li>'; // тег после текущей "крошки"

    $show_on_home = 0; // 1 - показывать "хлебные крошки" на главной странице, 0 - не показывать
    $show_home_link = 1; // 1 - показывать ссылку "Главная", 0 - не показывать
    $show_current = 1; // 1 - показывать название текущей страницы, 0 - не показывать
    $show_last_sep = 1; // 1 - показывать последний разделитель, когда название текущей страницы не отображается, 0 - не показывать
    /* === КОНЕЦ ОПЦИЙ === */

    global $post;
    $home_url = home_url('/');
    $link = '<li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">';
    $link .= '<a href="%1$s" class="breadcrumbs__link" itemprop="item"><span itemprop="name">%2$s</span><meta itemprop="position" content="%3$s" /></a>';
    $link .= '</li>';
    $parent_id = ($post) ? $post->post_parent : '';
    $home_link = sprintf($link, $home_url, $text['home'], 1);

    if (is_home() || is_front_page()) {
        if ($show_on_home) {
            echo $wrap_before . $home_link . $wrap_after;
        }
    } else {
        $position = 0;

        echo $wrap_before;

        if ($show_home_link) {
            $position += 1;
            echo $home_link;
        }

        if (is_category()) {
            $parents = get_ancestors(get_query_var('cat'), 'category');
            foreach (array_reverse($parents) as $cat) {
                $position += 1;
                if ($position > 1) {
                    echo $sep;
                }
                echo sprintf($link, get_category_link($cat), get_cat_name($cat), $position);
            }
            if (get_query_var('paged')) {
                $position += 1;
                $cat = get_query_var('cat');
                echo $sep . sprintf($link, get_category_link($cat), get_cat_name($cat), $position);
                echo $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_current) {
                    if ($position >= 1) {
                        echo $sep;
                    }
                    echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;
                } elseif ($show_last_sep) {
                    echo $sep;
                }
            }
        } elseif (is_search()) {
            if (get_query_var('paged')) {
                $position += 1;
                if ($show_home_link) {
                    echo $sep;
                }
                echo sprintf($link, $home_url . '?s=' . get_search_query(), sprintf($text['search'], get_search_query()), $position);
                echo $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_current) {
                    if ($position >= 1) {
                        echo $sep;
                    }
                    echo $before . sprintf($text['search'], get_search_query()) . $after;
                } elseif ($show_last_sep) {
                    echo $sep;
                }
            }
        } elseif (is_year()) {
            if ($show_home_link && $show_current) {
                echo $sep;
            }
            if ($show_current) {
                echo $before . get_the_time('Y') . $after;
            } elseif ($show_home_link && $show_last_sep) {
                echo $sep;
            }
        } elseif (is_month()) {
            if ($show_home_link) {
                echo $sep;
            }
            $position += 1;
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y'), $position);
            if ($show_current) {
                echo $sep . $before . get_the_time('F') . $after;
            } elseif ($show_last_sep) {
                echo $sep;
            }
        } elseif (is_day()) {
            if ($show_home_link) {
                echo $sep;
            }
            $position += 1;
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y'), $position) . $sep;
            $position += 1;
            echo sprintf($link, get_month_link(get_the_time('Y'), get_the_time('m')), get_the_time('F'), $position);
            if ($show_current) {
                echo $sep . $before . get_the_time('d') . $after;
            } elseif ($show_last_sep) {
                echo $sep;
            }
        } elseif (is_single() && !is_attachment()) {
            if (get_post_type() != 'post') {
                $position += 1;
                $post_type = get_post_type_object(get_post_type());
                if ($position > 1) {
                    echo $sep;
                }
                echo sprintf($link, get_post_type_archive_link($post_type->name), $post_type->labels->name, $position);
                if ($show_current) {
                    echo $sep . $before . get_the_title() . $after;
                } elseif ($show_last_sep) {
                    echo $sep;
                }
            } else {
                $cat = get_the_category();
                $catID = $cat[0]->cat_ID;
                $parents = get_ancestors($catID, 'category');
                $parents = array_reverse($parents);
                $parents[] = $catID;
                foreach ($parents as $cat) {
                    $position += 1;
                    if ($position > 1) {
                        echo $sep;
                    }
                    echo sprintf($link, get_category_link($cat), get_cat_name($cat), $position);
                }
                if (get_query_var('cpage')) {
                    $position += 1;
                    echo $sep . sprintf($link, get_permalink(), get_the_title(), $position);
                    echo $sep . $before . sprintf($text['cpage'], get_query_var('cpage')) . $after;
                } else {
                    if ($show_current) {
                        echo $sep . $before . get_the_title() . $after;
                    } elseif ($show_last_sep) {
                        echo $sep;
                    }
                }
            }
        } elseif (is_post_type_archive()) {
            $post_type = get_post_type_object(get_post_type());
            if (get_query_var('paged')) {
                $position += 1;
                if ($position > 1) {
                    echo $sep;
                }
                echo sprintf($link, get_post_type_archive_link($post_type->name), $post_type->label, $position);
                echo $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_home_link && $show_current) {
                    echo $sep;
                }
                if ($show_current) {
                    echo $before . $post_type->label . $after;
                } elseif ($show_home_link && $show_last_sep) {
                    echo $sep;
                }
            }
        } elseif (is_attachment()) {
            $parent = get_post($parent_id);
            $cat = get_the_category($parent->ID);
            $catID = $cat[0]->cat_ID;
            $parents = get_ancestors($catID, 'category');
            $parents = array_reverse($parents);
            $parents[] = $catID;
            foreach ($parents as $cat) {
                $position += 1;
                if ($position > 1) {
                    echo $sep;
                }
                echo sprintf($link, get_category_link($cat), get_cat_name($cat), $position);
            }
            $position += 1;
            echo $sep . sprintf($link, get_permalink($parent), $parent->post_title, $position);
            if ($show_current) {
                echo $sep . $before . get_the_title() . $after;
            } elseif ($show_last_sep) {
                echo $sep;
            }
        } elseif (is_page() && !$parent_id) {
            if ($show_home_link && $show_current) {
                echo $sep;
            }
            if ($show_current) {
                echo $before . get_the_title() . $after;
            } elseif ($show_home_link && $show_last_sep) {
                echo $sep;
            }
        } elseif (is_page() && $parent_id) {
            $parents = get_post_ancestors(get_the_ID());
            foreach (array_reverse($parents) as $pageID) {
                $position += 1;
                if ($position > 1) {
                    echo $sep;
                }
                echo sprintf($link, get_page_link($pageID), get_the_title($pageID), $position);
            }
            if ($show_current) {
                echo $sep . $before . get_the_title() . $after;
            } elseif ($show_last_sep) {
                echo $sep;
            }
        } elseif (is_tag()) {
            if (get_query_var('paged')) {
                $position += 1;
                $tagID = get_query_var('tag_id');
                echo $sep . sprintf($link, get_tag_link($tagID), single_tag_title('', false), $position);
                echo $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_home_link && $show_current) {
                    echo $sep;
                }
                if ($show_current) {
                    echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
                } elseif ($show_home_link && $show_last_sep) {
                    echo $sep;
                }
            }
        } elseif (is_author()) {
            $author = get_userdata(get_query_var('author'));
            if (get_query_var('paged')) {
                $position += 1;
                echo $sep . sprintf($link, get_author_posts_url($author->ID), sprintf($text['author'], $author->display_name), $position);
                echo $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_home_link && $show_current) {
                    echo $sep;
                }
                if ($show_current) {
                    echo $before . sprintf($text['author'], $author->display_name) . $after;
                } elseif ($show_home_link && $show_last_sep) {
                    echo $sep;
                }
            }
        } elseif (is_404()) {
            if ($show_home_link && $show_current) {
                echo $sep;
            }
            if ($show_current) {
                echo $before . $text['404'] . $after;
            } elseif ($show_last_sep) {
                echo $sep;
            }
        } elseif (has_post_format() && !is_singular()) {
            if ($show_home_link && $show_current) {
                echo $sep;
            }
            echo get_post_format_string(get_post_format());
        }

        echo $wrap_after;
    }
} // end of dimox_breadcrumbs()

/* количество просмотров */

function getPostViews($postID)
{
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count=='') {
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 просмотров";
    }
    return ' Просмотров: '.$count;
}
function setPostViews($postID)
{
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count=='') {
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

function dateToRussian($date)
{
    $month= [
        "january"=>__("января", 'VAB'),
        "february"=>__("февраля", 'VAB'),
        "march"=>__("марта", 'VAB'),
        "april"=>__("апреля", 'VAB'),
        "may"=>__("мая", 'VAB'),
        "june"=>__("июня", 'VAB'),
        "july"=>__("июля", 'VAB'),
        "august"=>__("августа", 'VAB'),
        "september"=>__("сентября", 'VAB'),
        "october"=>__("октября", 'VAB'),
        "november"=>__("ноября", 'VAB'),
        "december"=>__("декабря", 'VAB')];
    $days=array(
        "monday"=>__("Понедельник", 'VAB'),
        "tuesday"=>__("Вторник", 'VAB'),
        "wednesday"=>__("Среда", 'VAB'),
        "thursday"=>__("Четверг", 'VAB'),
        "friday"=>__("Пятница", 'VAB'),
        "saturday"=>__("Суббота", 'VAB'),
        "sunday"=>__("Воскресенье", 'VAB'));
    return str_replace(array_merge(array_keys($month), array_keys($days)), array_merge($month, $days), strtolower($date));
}

// Считать термины в типе записей
function get_term_post_count_by_type($term, $taxonomy, $type)
{
    $args = array(
        'fields' =>'ids',
        'posts_per_page' => -1,
        'post_type' => $type,
        'tax_query' => array(
            array(
                'taxonomy' => $taxonomy,
                'field' => 'slug',
                'terms' => $term
            )
        )
    );
    $ps = get_posts($args);
    if (count($ps) > 0) {
        return count($ps);
    } else {
        return 0;
    }
}

/**
 * Склонение слова после числа.
 *
 * Примеры вызова:
 * num_decline( $num, 'книга,книги,книг' )
 * num_decline( $num, [ 'книга','книги','книг' ] )
 * num_decline( $num, 'книга', 'книги', 'книг' )
 * num_decline( $num, 'книга', 'книг' )
 *
 * @param  int|string    $number  Число после которого будет слово. Можно указать число в HTML тегах.
 * @param  string|array  $titles  Варианты склонения или первое слово для кратного 1.
 * @param  string        $param2  Второе слово, если не указано в параметре $titles.
 * @param  string        $param3  Третье слово, если не указано в параметре $titles.
 *
 * @return string 1 книга, 2 книги, 10 книг.
 *
 * @version 2.0
 */
function num_decline($number, $titles, $param2 = '', $param3 = '')
{

    if ($param2) {
        $titles = [ $titles, $param2, $param3 ];
    }

    if (is_string($titles)) {
        $titles = preg_split('/, */', $titles);
    }

    if (empty($titles[2])) {
        $titles[2] = $titles[1]; // когда указано 2 элемента
    }

    $cases = [ 2, 0, 1, 1, 1, 2 ];

    $intnum = abs(intval(strip_tags($number)));

    return "$number ". $titles[ ($intnum % 100 > 4 && $intnum % 100 < 20) ? 2 : $cases[min($intnum % 10, 5)] ];
}

/**
 * Функция фильтрует строку и устанавливает формат вывода телефонного номера
 * @param string $phone Строка с телефоном
 * @return string
 */

function set_format_phone($phone)
{
    $phone = preg_replace("/[^0-9]/", "", $phone);

    if (strlen($phone) == 7) {
        $phone = preg_replace("/([0-9]{3})([0-9]{2})([0-9]{2})/", "$1-$2-$3", $phone);
    } elseif (strlen($phone) == 10) {
        $phone = preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1)$2-$3", $phone);
    } elseif (strlen($phone) == 11) {
        $phone = preg_replace("/([0-9])([0-9]{3})([0-9]{3})([0-9]{4})/", "$1($2)$3-$4", $phone);
        $first = substr($phone, 0, 1);
        if (in_array($first, array(7, 8))) {
            $phone = '+7'. substr($phone, 1);
        }
    }

    return $phone;
}

function acf_get_field_key($field_name, $post_id)
{
    global $wpdb;
    $acf_fields = $wpdb->get_results($wpdb->prepare("SELECT ID,post_parent,post_name FROM $wpdb->posts WHERE post_excerpt=%s AND post_type=%s", $field_name, 'acf-field'));
    // get all fields with that name.
    switch (count($acf_fields)) {
        case 0: // no such field
            return false;
        case 1: // just one result.
            return $acf_fields[0]->post_name;
    }
    // result is ambiguous
    // get IDs of all field groups for this post
    $field_groups_ids = array();
    $field_groups = acf_get_field_groups(array(
        'post_id' => $post_id,
    ));
    foreach ($field_groups as $field_group) {
        $field_groups_ids[] = $field_group['ID'];
    }

    // Check if field is part of one of the field groups
    // Return the first one.
    foreach ($acf_fields as $acf_field) {
        if (in_array($acf_field->post_parent, $field_groups_ids)) {
            return $acf_field->post_name;
        }
    }
    return false;
}

function js_variables()
{
    $variables = array (
        'ajax_url' => admin_url('admin-ajax.php')
    );
    echo '<script type="text/javascript">window.global = ' . json_encode($variables) .';</script>';
}
add_action('wp_head', 'js_variables');

// numbered pagination
function pagination($pages = '', $range = 4)
{
    $showitems = ($range * 2)+1;

    global $paged;
    if (empty($paged)) {
        $paged = 1;
    }

    if ($pages == '') {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if (!$pages) {
            $pages = 1;
        }
    }

    if (1 != $pages) {
        echo "<ul class=\"pagination\">";
        for ($i=1; $i <= $pages; $i++) {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
                echo ($paged == $i)? "<li class=\"pagination__item pagination__item--current\">".$i."</li>":"<li class=\"pagination__item\"><a href='".get_pagenum_link($i)."'>".$i."</a></li>";
            }
        }
        echo "</ul>\n";
    }
}

pll_register_string('Blog Plural', 'Блог');


add_action('wp_ajax_callback_action', ['\App\Controllers\FormsController', 'callback_action']);
add_action('wp_ajax_nopriv_callback_action', ['\App\Controllers\FormsController', 'callback_action']);

add_action('wp_ajax_subscribe_action', ['\App\Controllers\FormsController', 'subscribe_action']);
add_action('wp_ajax_nopriv_subscribe_action', ['\App\Controllers\FormsController', 'subscribe_action']);

add_action('wp_ajax_program_action', ['\App\Controllers\FormsController', 'program_action']);
add_action('wp_ajax_nopriv_program_action', ['\App\Controllers\FormsController', 'program_action']);

add_action('wp_ajax_quiz_action', ['\App\Controllers\FormsController', 'quiz_action']);
add_action('wp_ajax_nopriv_quiz_action', ['\App\Controllers\FormsController', 'quiz_action']);
