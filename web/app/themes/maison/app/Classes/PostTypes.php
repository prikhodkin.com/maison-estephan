<?php

namespace App\Classes;

final class PostTypes
{
    const BlOG = 'blog';
    const SERVICES = 'services';
    const FORMS = 'forms';
}
