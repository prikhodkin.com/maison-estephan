// import external dependencies
import 'jquery';
import 'slick-carousel';
import 'malihu-custom-scrollbar-plugin';
// Import everything from autoload
import './autoload/**/*'

import Tabs from './util/Tabs';

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';
import {Popup, PopupThanks} from './util/popup';
import Quiz from './util/Quiz';
/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
    common,
  // Home page
    home,
  // About Us page, note the change from about-us to aboutUs.
    aboutUs,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
const initSlider = () => {
    $('.problems__list').slick({
        dots: false,
        adaptiveHeight: true,
        nextArrow: '.problems__control--next',
        prevArrow: '.problems__control--prev',
    })

  $('.problems .problems__lenght').html($('.problems__list').slick('getSlick').slideCount);

  $('.problems__list').on('afterChange', function (event, slick, currentSlide) {
    $('.problems .problems__current').html(currentSlide + 1);
  });
}

initSlider();

const popups = document.querySelectorAll('.popup');
const popupThank = document.querySelector('.popup--thanks');

popups.forEach(function (popup) {
    new Popup(popup);
});


const ajaxSend = (url, method, data) => {
    return fetch(url, {
        method: method,
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
        },
        body: data,
    })
    .then(
        function (response) {
            if (response.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' +
                response.status);
                return;
            }
            console.log('success');
            return response.json();
        }
    )
    .catch(error => console.log(error))
}


const send = (evt) => {
    evt.preventDefault();
    const {target} = evt;
    const formData = new FormData(target);
    ajaxSend(window.global.ajax_url, 'post', formData).then((data) => console.log(data));
    new PopupThanks(popupThank).openPopup();
    target.reset();
}

const forms = document.querySelectorAll('form');

forms.forEach(function (item) {
    item.addEventListener('submit', function (evt) {
        send(evt);
    })
})

$('.reviews__list').slick({
  dots: false,
  nextArrow: '.reviews__control--next',
  prevArrow: '.reviews__control--prev',
  adaptiveHeight: true,
})

$('.reviews .reviews__lenght').html($('.reviews__list').slick('getSlick').slideCount)
$('.reviews__list').on('afterChange', function(event, slick, currentSlide){
  $('.reviews .reviews__current').html(currentSlide + 1);
});

if($(window).width() >= 1280) {
  $('.about__scroll').mCustomScrollbar({
    scrollButtons:{enable:true},
    axis:'y',
    theme: 'dark',
    mouseWheel: {
      enable: true,
    },
  });
}



const tabs = document.querySelector('[data-tabs]');
if(tabs) {
  new Tabs(tabs);
}

const playBtns = document.querySelectorAll('.video__play');



const play = (button) => {
  const PAUSE_CLASS = 'video__play--pause';
  const _this = button;
  const parent = _this.parentNode;
  const video = parent.querySelector('video');
  let isActive = true;


  _this.addEventListener('click', function () {
    if(isActive) {
      // _this.classList.add(PAUSE_CLASS);
      video.play();
      video.setAttribute('controls', '');
      _this.style.display = 'none';
      isActive = !isActive;
    }

    video.addEventListener('click', () => {
      video.removeAttribute('controls');
      video.pause();
      if(!isActive) {
        _this.style.display = 'flex';
        _this.classList.add(PAUSE_CLASS);
        setTimeout(() => {
          _this.classList.remove(PAUSE_CLASS);
        }, 300)
      }
      isActive = true;
    })

    video.addEventListener('ended', () => {
      isActive = true;
      console.log(isActive)
      _this.style.display = 'flex';
      // _this.classList.remove(PAUSE_CLASS)
    })
  })
}


if(playBtns) {
  playBtns.forEach((item) => {
    play(item);
  })
}

const quiz = document.querySelector('[data-quiz]');

if(quiz) {
   new Quiz(quiz);
}

$(window).on('scroll', function () {
  if($(this).scrollTop() >= $('.header').innerHeight()) {
    $('.header').addClass('header--sticky');
  } else {
    $('.header').removeClass('header--sticky');
  }
});


$('.contacts__partner').slick({
  dots: false,
  nextArrow: '.reviews__control--next',
  prevArrow: '.reviews__control--prev',
  adaptiveHeight: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1279,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: false,
      },
    },
  ],
})
