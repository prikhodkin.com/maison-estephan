export default {
    init() {


        const showMenu = (target, menu) => {
            target.addEventListener('click', () => {
                $(menu).fadeToggle();
            })
        }
        const languageArrow = document.querySelector('.language__arrow');
        const menuButton = document.querySelector('.menu__open');
        const menuClose = document.querySelector('.menu__close');
        const menu = document.querySelector('.menu__inner');

        showMenu(menuButton, menu);
        showMenu(menuClose, menu);

        const phoneArrow = document.querySelector('.header__phones-arrow');
        languageArrow.addEventListener('click', () => {
            languageArrow.parentNode.classList.toggle('language--active');
        });
        phoneArrow.addEventListener('click', () => {
            phoneArrow.classList.toggle('header__phones-arrow--active')
            phoneArrow.parentNode.classList.toggle('header__phones--active')
        })
    },
    finalize() {
      // JavaScript to be fired on all pages, after page specific JS is fired
    },
};
