class Quiz {
  constructor(target) {
    this._target = target;
    this.init();
  }

  init() {
    this.findStage();
    this.nextStage();
  }

  findStage() {
    // const list = this._target.querySelectorAll('[data-list]');
    // this.showLength(list.length);
  }

  nextStage() {
    const button = this._target.querySelector('[data-button]');
    button.addEventListener('click', (evt) => {
      evt.preventDefault();
      const currentList = this._target.querySelector('.quiz__content--active');
      const currentId = currentList.getAttribute('data-list');
      const nextList = this._target.querySelector(`[data-list="${Number.parseInt(currentId) + 1}"]`);
      if(nextList) {
        currentList.classList.remove('quiz__content--active');
        nextList.classList.add('quiz__content--active');
        this.showCurrent(Number.parseInt(currentId) + 1);
        if(currentId > 4) {
          const controls = this._target.querySelector('.quiz__control');
          controls.style.display = 'none';
          currentList.classList.remove('quiz__content--active');
          this.addAnswer();
        }
      }
    })
  }

  addAnswer() {
    const form = this._target.querySelector('.js-quiz');
    let arr = [];
    this.findAnswer().forEach((item) => {
      arr.push(this.fillHiddenInputs(item));

    })
    arr.reverse().forEach((item) => {
      form.insertAdjacentHTML('afterbegin', item);
    })
  }

  fillHiddenInputs(item) {
    return `<input type="hidden" name="question[]" value="${item}">`
  }

  showThanks() {
    const form = this._target.querySelector('[data-list="5"]');
    const thx = this._target.querySelector('[data-list="6"]');
    // const button = this._target.querySelector('.quiz__submit');
    form.classList.remove('quiz__content--active');
    thx.classList.add('quiz__content--active');
    // button.addEventListener(`click`, (evt) => {
    //   evt.preventDefault();
    //
    // })

  }
  showLength(stages) {
    const length = this._target.querySelector('[data-length]');
    length.innerHTML = stages;
  }

  showCurrent(id) {
    const current = this._target.querySelector('[data-current]');
    current.innerHTML = id;
  }

  findAnswer() {
    const inputList = this._target.querySelectorAll('input');
    let answer = [];
    inputList.forEach((item) => {
      if(item.checked && item.type === 'checkbox') {
        answer.push(item.value);
      }
    })
    return answer;
  }

}

export default Quiz;
