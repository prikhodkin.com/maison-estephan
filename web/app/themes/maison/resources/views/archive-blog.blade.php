@extends('layouts.app')
@section('content')
  @include('partials.breadcrumbs')
  <div class="layout">
    <h1 class="layout__title extratitle">{{pll__('Блог Maison Estephan', 'Maison')}}</h1>
    <p class="layout__desc">{{pll__('Недавние записи', 'Maison')}}</p>
    <ul class="layout__list">
      @while($posts->have_posts())
        {{$posts->the_post()}}
        @include('partials.article')
      @endwhile
    </ul>
    {{pagination($posts->max_num_pages)}}
    {{wp_reset_postdata()}}
  </div>
  @include('partials.blocks.subscribe.subscribe')
  @include('partials.question.question')
@endsection
