@extends('layouts.app')

@section('content')
  <div class="layout layout--detail">
    <h1 class="layout__title title">{!! get_the_title() !!}</h1>
    <div class="layout__content">
      {!! the_content() !!}
    </div>
  </div>
@endsection
