{{--
  Template Name: Контакты
--}}

@extends('layouts.app')

@section('content')
  @include('partials.breadcrumbs')
  <div class="layout">
    <h1 class="layout__title extratitle">{{the_title()}}</h1>
    <div class="contacts">
      <div class="contacts__row">
        <div class="contacts__left question">
          <ul class="question__list">
            <li class="question__item question__item--email">
              <a href="mailto:{{get_field('email', 'option')}}">{{get_field('email', 'option')}}</a>
            </li>
            <li class="question__item question__item--phone">
              <a href="tel:{{App::NormalizePhone(get_field('phone-ru', 'option'))}}"><span>(RU)</span> {!! get_field('phone-ru', 'option') !!}</a>
            </li>
            <li class="question__item question__item--phone">
              <a href="tel:{{App::NormalizePhone(get_field('phone-fr', 'option'))}}"><span>(FR)</span> {!! get_field('phone-fr', 'option') !!}</a>
            </li>
          </ul>
          <ul class="contacts__social social">
            {!! App::printRepeatorField('social', 'partials.social.social-item', 'option') !!}
          </ul>
        </div>
        <div class="contacts__right">
          <h2 class="contacts__subtitle">{{pll__('Наши партнеры', 'Maison')}}</h2>
          <div class="contacts__slider">
            <ul class="contacts__partner partner">
              {!! App::printRepeatorField('partner', 'partials.blocks.contacts.partner.partner-item') !!}
            </ul>
            <div class="reviews__controls">
              <button class="reviews__control reviews__control--prev">
                @include('partials.icons.arrow-left')
              </button>
              <button class="reviews__control reviews__control--next">
                @include('partials.icons.arrow-right')
              </button>
            </div>
          </div>
        </div>
      </div>
      <p class="contacts__title extratitle">{{pll__('Если вы', 'Maison')}}</p>
      <ul class="contacts__list">
        {!! App::printRepeatorField('category', 'partials.blocks.contacts.category.contacts-item') !!}
      </ul>
      <p class="contacts__text">{{pll__('Пишите нам, будем рады будущему партнерству', 'Maison')}}</p>
    </div>
  </div>
@endsection
