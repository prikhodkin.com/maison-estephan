@extends('layouts.app')

@section('content')
  @include('partials.breadcrumbs')
  <div class="layout layout--detail">
  @while(have_posts()) @php the_post() @endphp

    <span class="layout__date">{{get_the_date('d.m.Y')}}</span>
    <h1 class="layout__title title">{!! get_the_title() !!}</h1>
    <div class="layout__content">
      {!! the_content() !!}
    </div>

  @endwhile
  @if($posts->have_posts())
    <p class="layout__desc">{{pll__('Эти статьи могут вас заинтересовать', 'Maison')}}</p>
    <ul class="layout__list">
      @while($posts->have_posts())
        {{$posts->the_post()}}
        @include('partials.article')
      @endwhile
    </ul>
  </div>
  @endif
  {{wp_reset_postdata()}}
  @include('partials.question.question')
@endsection
