{{--
  Template Name: Главная
--}}

@extends('layouts.app')

@section('content')
  <div class="layout">
    @include('partials.blocks.promo.promo')
    @include('partials.blocks.video.video', [
       'title' => get_field('video-title', 'option'),
       'desc' => get_field('video-desc', 'option'),
       'poster' => get_field('video-poster', 'option'),
       'src' => get_field('video-file', 'option')
     ])
    @include('partials.blocks.why.why')
    @include('partials.blocks.quiz.quiz')
    @include('partials.blocks.our-services.our-services')
    @include('partials.blocks.help.help')
    @include('partials.blocks.reviews.reviews')
    @if($posts->have_posts())
      <div class="layout__read">
        <p class="layout__title title">{{pll__('Читайте полезные статьи в нашем блоге', 'Maison')}}</p>
        <ul class="layout__list">
          @while($posts->have_posts())
            {{$posts->the_post()}}
            @include('partials.article')
          @endwhile
        </ul>
        <a href="{{get_post_type_archive_link('blog')}}" class="program__button button">
          {{pll__('Перейти в блог', 'Maison')}}
          <span class="button__icon">
            @include('partials.icons.arrow')
          </span>
        </a>
      </div>
    @endif
    {{wp_reset_postdata()}}
    @include('partials.question.question')
  </div>
@endsection
