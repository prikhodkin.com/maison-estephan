{{--
  Template Name: Услуги
--}}

@extends('layouts.app')
@section('content')
  @include('partials.breadcrumbs')
  <div class="layout">
    <h1 class="layout__title extratitle">{{pll__('Наши услуги', 'Maison')}}</h1>
    <section class="information">
      <div class="information__content">
        <h2 class="information__title">{{get_field('information-title')}}</h2>
        {!! get_field('information-text') !!}
      </div>
      <div class="information__img">
        <img src="{{get_field('information-img')}}" alt="{{get_field('information-title')}}">
      </div>
    </section>
    <section class="other-programs" id="consulting">
      <ul class="other-programs__list">
        @while($other->have_posts())
          {{$other->the_post()}}
          @include('partials.other-programs.other-programs-item')
        @endwhile
        {{wp_reset_query()}}
      </ul>
    </section>
    <section class="programs" id="programs">
      <h2 class="programs__title title">{{get_field('programs-title')}}</h2>
      <p class="programs__desc">{{get_field('programs-desc')}}</p>
      <ul class="programs__list">
        @while($services->have_posts())
          {{$services->the_post()}}
          @include('partials.blocks.programs.programs-item')
        @endwhile
        {{wp_reset_query()}}
      </ul>
    </section>
    @include('partials.blocks.problems.problems')
  </div>

  @include('partials.question.question')
@endsection
