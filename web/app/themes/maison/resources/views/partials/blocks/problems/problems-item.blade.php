<li class="problems__item">
  <div class="problems__inner">
    <ul class="problems__sub-list">
      <li class="problems__sub-item problems__sub-item--problem">
        <div>
          <p class="problems__header">{{pll__('Проблема:', 'Maison')}}</p>
          <p class="problems__text">{{$row['problems-item']['problems']}}</p>
        </div>
      </li>
      <li class="problems__sub-item problems__sub-item--action">
        <div>
          <p class="problems__header">{{pll__('Какие меры были предприняты:', 'Maison')}}</p>
          <p class="problems__text">{{$row['problems-item']['measures']}}</p>
        </div>
      </li>
    </ul>
    <div class="problems__visual">
      <img src="{{$row['problems-item']['image']}}" alt="{{$row['problems-item']['problems']}}" class="problems__img">
      <div class="problems__info">
        <div>
          <p class="problems__caption">{{pll__('Что в итоге получилось:', 'Maison')}}</p>
          <p class="problems__desc">{{$row['problems-item']['total']}}</p>
        </div>
      </div>
    </div>
  </div>
</li>
