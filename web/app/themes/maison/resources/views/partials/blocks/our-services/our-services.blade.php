<section class="our-services">
  <h2 class="our-services__title extratitle">{{get_field('our-title')}}</h2>
  <p class="our-services__desc">{{get_field('our-desc')}}</p>
  <ul class="our-services__list">
    {!! App::printRepeatorField('our-list', 'partials.blocks.our-services.our-services-item') !!}
  </ul>
</section>
