<div class="quiz__content @if($key === 0) quiz__content--active @endif " data-list="{{$key + 1}}">
  <h2 class="quiz__title"><span>{{$key + 1}}.</span> {{$row['quiz-title']}}</h2>
  <ul class="quiz__list">
    @foreach($row['quiz-items'] as $index => $item)
      <li class="quiz__item">
        <input type="checkbox" id="radio{{$key}}{{$index}}" name="radio{{$key}}{{$index}}" value="{{$item['text']}}" @if($index === 0) checked @endif>
        <label for="radio{{$key}}{{$index}}" class="quiz__label">
          <div class="quiz__circle"></div>
          @if($item['img'])
            <img src="{{$item['img']}}" class="quiz__icon">
          @endif
          <p class="quiz__header">{{$item['text']}}</p>
        </label>
      </li>
    @endforeach
  </ul>
</div>
