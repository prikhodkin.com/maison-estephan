<section class="help">
  <div class="help__content">
    <h2 class="help__title extratitle">{{get_field('help-title')}}</h2>
    <p class="help__desc">{{get_field('help-desc')}}</p>
    <p class="help__place">{{get_field('help-place')}}</p>
    <a href="javascript://" onclick="Calendly.initPopupWidget({url: 'https://calendly.com/madina-estephan?primary_color=4ac986'});return false;" class="help__button button">
      {{pll__('Посмотреть расписание', 'Maison')}}
      <span class="button__icon">
        @include('partials.icons.arrow')
      </span>
    </a>
  </div>
  <img src="{{get_field('help-img')}}" alt="{{get_field('help-title')}}" class="help__img">
</section>
