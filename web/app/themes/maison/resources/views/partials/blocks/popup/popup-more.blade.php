<div class="popup popup--more" data-popup="more{{get_the_ID()}}">
  <div class="popup__wrapper">
    <div class="popup__content">
      <button class="popup__close">
        @include('partials.icons.close')
      </button>
      <h2 class="popup__title title">{{the_title()}}</h2>
      <p class="popup__subtitle">{{get_field('program-subtitle')}}</p>
      <div class="popup__text-block">
        {!! get_field('program-list-block') !!}
      </div>
      @if(get_field('program-week'))
        <p class="popup__line">
          @include('partials.icons.calendar')
          {{get_field('program-week')}}
        </p>
      @endif
      @if(get_field('program-group'))
        <p class="popup__line">
          @include('partials.icons.man')
          {{get_field('program-group')}}
        </p>
      @endif
      <div class="popup__block">
        {!! get_field('program-more') !!}
      </div>
      <button class="popup__button button get-popup" data-popup="order{{get_the_ID()}}">
        {{pll__('Записаться', 'Maison')}}
        <div class="button__icon">
          @include('partials.icons.pen')
        </div>
      </button>
    </div>
  </div>
</div>
