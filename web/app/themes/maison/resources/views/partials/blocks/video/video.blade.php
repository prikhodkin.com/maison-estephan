<section class="video">
  <h2 class="video__title extratitle">{{$title}}</h2>
  @if($desc !== '')
    <p class="video__description">{{$desc}}</p>
  @endif
  <div class="video__wrapper">
    <video src="{{$src}}" poster="{{$poster}}"></video>
    <button class="video__play">
      <span class="video__inner">{{pll__('Нажмите для просмотра', 'Maison')}}</span>
    </button>
  </div>
</section>
