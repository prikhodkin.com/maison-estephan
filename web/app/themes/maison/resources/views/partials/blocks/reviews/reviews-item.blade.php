<li class="reviews__item">
  <p class="reviews__name">{{$row['reviews-item']['reviews-name']}}</p>
  <p class="reviews__position">{{$row['reviews-item']['reviews-position']}}</p>
  <div class="reviews__content">
    {!! $row['reviews-item']['reviews-content'] !!}
  </div>
</li>
