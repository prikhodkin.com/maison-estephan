<div class="quiz__control">
  <a href="" class="quiz__button button button--cream" data-button>
    {{pll__(' К следующему вопросу', 'Maison')}}
    <div class="button__icon">
      @include('partials.icons.arrow')
    </div>
  </a>
  <div class="quiz__counter">{{pll__('Вопрос', 'Maison')}} <span class="quiz__current" data-current>1</span> {{pll__('из', 'Maison')}} <span class="quiz__length" data-length>5</span></div>
</div>
