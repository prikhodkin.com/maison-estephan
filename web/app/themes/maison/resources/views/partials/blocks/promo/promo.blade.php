<section class="promo">
  <div class="promo__content">
    <div class="promo__title extratitle">{{get_field('promo-title')}}</div>
    <p class="promo__subtitle">{{get_field('promo-caption')}}</p>
    <div class="promo__desc">{!! get_field('promo-text') !!}</div>
    <a href="javascript://" class="promo__button button get-popup" data-popup="consultation">
      {{pll__('Давайте обсудим!', 'Maison')}}
      <div class="button__icon">
        @include('partials.icons.pen')
      </div>
    </a>
  </div>
  <div class="promo__people">
    <picture>
      <source media="(min-width: 1280px)" srcset="{{get_field('promo-img-desk')}}">
      <img src="{{get_field('promo-img-mobile')}}" alt="{{get_field('promo-title')}}" class="promo__img">
    </picture>

    <div class="promo__table">
      <p class="promo__name">{{get_field('promo-name')}}</p>
      <p class="promo__position">{{get_field('promo-position')}}</p>
    </div>
  </div>
</section>
