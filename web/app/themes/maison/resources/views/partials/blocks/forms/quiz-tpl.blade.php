<h1 style='padding: 10px; border: #e9e9e9 1px solid;'>Квиз опрос</h1>
<h3 style='padding: 10px; border: #e9e9e9 1px solid;'>Email</h3>
<p style='padding: 10px; border: #e9e9e9 1px solid;'>{{wp_strip_all_tags($query['email'])}}</p>
<h3 style='padding: 10px; border: #e9e9e9 1px solid;'>Ответы</h3>
<ul>
  <li><p>Ответ на вопрос №1 <b>{{wp_strip_all_tags($query['question1'])}}</b></p></li>
  <li><p>Ответ на вопрос №2 <b>{{wp_strip_all_tags($query['question2'])}}</b></p></li>
  <li><p>Ответ на вопрос №3 <b>{{wp_strip_all_tags($query['question3'])}}</b></p></li>
  <li><p>Ответ на вопрос №4 <b>{{wp_strip_all_tags($query['question4'])}}</b></p></li>
  <li><p>Ответ на вопрос №5 <b>{{wp_strip_all_tags($query['question5'])}}</b></p></li>
</ul>
