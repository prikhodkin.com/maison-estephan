<div class="about__row">
  <div class="about__wrapper">
    <div class="about__name">{{get_field('second-name')}}</div>
    <p class="about__position">{{get_field('second-position')}}</p>
    <div class="about__scroll">
      <div class="about__block">
        {!! get_field('second-text') !!}
      </div>
    </div>
  </div>
  <img src="{{get_field('second-img')}}" alt="{{get_field('second-name')}}" class="about__photo">
</div>
