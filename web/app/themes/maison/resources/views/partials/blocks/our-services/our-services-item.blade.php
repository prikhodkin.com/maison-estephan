<li class="our-services__item">
  <div class="our-services__body">
    <p class="our-services__header">{{$row['our-header']}}</p>
    <div class="our-services__content">
      {!! $row['our-content'] !!}
    </div>
    <a href="{{$row['our-link']}}" class="our-services__button button">
      @if($key === 0)
        {{pll__('Подробнее о консультациях', 'Maison')}}
      @else
        {{pll__('Подробнее о программах', 'Maison')}}
      @endif
      <span class="button__icon">
        @include('partials.icons.arrow')
      </span>
    </a>
  </div>
  <img src="{{$row['our-img']}}" alt="{{$row['our-header']}}" class="our-services__img">
</li>
