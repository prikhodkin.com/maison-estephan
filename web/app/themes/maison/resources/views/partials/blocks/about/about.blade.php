<section class="about" id="about">
  <h2 class="about__title extratitle">{{get_field('about-title')}}</h2>
  <div class="about__content">
    <div class="about__name">{{get_field('about-name')}}</div>
    <p class="about__position">{{get_field('about-position')}}</p>
    <div class="about__wrapper">
      <div class="about__scroll">
        <div class="about__text">{!!  get_field('about-text')!!}</div>
      </div>
    </div>
    <img src="{{get_field('about-img')}}" alt="{{get_field('about-name')}}" class="about__img">
  </div>
  <div class="about__tabs tabs" data-tabs>
    <ul class="tabs__list">
      {!! App::printRepeatorField('about-list', 'partials.blocks.about.tabs-item') !!}
    </ul>
    {!! App::printRepeatorField('about-list', 'partials.blocks.about.tabs-content') !!}
  </div>
  @include('partials.blocks.about.about-row')
</section>
