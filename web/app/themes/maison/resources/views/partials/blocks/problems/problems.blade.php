<section class="problems" id="case">
  <h2 class="problems__title title">{{get_field('problems-title')}}</h2>
  <ul class="problems__list">
    {!! App::printRepeatorField('problems-list', 'partials.blocks.problems.problems-item') !!}
  </ul>
  <a href="javascript://" class="problems__button button get-popup" data-popup="consultation">
    {{pll__('У меня похожая проблема!', 'Maison')}}
    <div class="button__icon">
      @include('partials.icons.pen')
    </div>
  </a>
  <div class="problems__controls">
    <button class="problems__control problems__control--prev">
      @include('partials.controls.controls-prev')
    </button>
    <div class="problems__counter">
      <span class="problems__current">1</span> из <span class="problems__lenght">5</span>
    </div>
    <button class="problems__control problems__control--next">
      @include('partials.controls.controls-next')
    </button>
  </div>
</section>
