<div class="quiz__content" data-list="6">
  <h2 class="quiz__title title">{{pll__('Мы учли ваши данные', 'Maison')}}</h2>
  <p class="quiz__description">{{pll__('Оставьте свой e-mail и мы пришлём вам бесплатное руководство по стрессу', 'Maison')}}</p>
  <form class="quiz__form form js-quiz">
    <input type="hidden" name="action" value="quiz_action">
    <input type="hidden" name="name" value="{{the_title()}}">
    <div class="subscribe" >
      <div class="subscribe__input">
        <input autocomplete="off" type="email" name="email" required placeholder="{{pll__('Ваш e-mail', 'Maison')}}">
      </div>
    </div>
    <button type="submit" class="quiz__submit button">
      {{pll__('Получить руководство', 'Maison')}}
    </button>
  </form>
  <label class="popup__label">
    <input type="checkbox" checked>
    <span class="popup__checkbox"></span>
    {{pll__('Даю согласие на обработку ', 'Maison')}}
    <a href="{{get_privacy_policy_url()}}">&nbsp;{{pll__(' персональных данных', 'Maison')}}</a>
  </label>
</div>
