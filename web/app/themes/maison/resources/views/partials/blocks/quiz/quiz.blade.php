<section class="quiz" data-quiz>
  <h2 class="title">{{get_field('quiz-t')}}</h2>
  {!! App::printRepeatorField('quiz-list', 'partials.blocks.quiz.quiz-item') !!}
  @include('partials.blocks.quiz.quiz-form')
  @include('partials.blocks.quiz.quiz-thx')
  @include('partials.blocks.quiz.quiz-controls')
</section>
