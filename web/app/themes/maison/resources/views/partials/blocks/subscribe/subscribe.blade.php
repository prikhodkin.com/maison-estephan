<section class="subscribe-form">
  <div class="subscribe-form__content">
    <h2 class="subscribe-form__title title">{{get_field('subscribe-form-title', 'option')}}</h2>
    <p class="subscribe-form__name">{{get_field('subscribe-form-name', 'option')}}</p>
    <p class="subscribe-form__text">{{get_field('subscribe-form-text', 'option')}}</p>
    @include('partials.subscribe.subscribe', ['class'=>''])
  </div>
  <div class="subscribe-form__img">
    <img src="{{get_field('subscribe-form-img', 'option')}}" alt="{{get_field('subscribe-form-title', 'option')}}">
  </div>
</section>
