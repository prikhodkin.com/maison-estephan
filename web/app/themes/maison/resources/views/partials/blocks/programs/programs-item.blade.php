<li class="programs__item program" id="{!! str_replace(" ", "-", strtolower(get_the_title())) !!}">
  <div class="program__top">
    <div class="program__img" style="background-image: url({{get_field('program-bg')}})">
      <img src="{{get_field('program-logo')}}" alt="" class="program__logo">
    </div>
    <div class="program__body">
      <h3 class="program__title">{{the_title()}}</h3>
      <p class="program__desc">{{get_field('program-desc')}}</p>
      <div class="program__content">
        {!! get_field('program-content') !!}
      </div>
    </div>
  </div>
  <div class="program__bottom">
    <p class="program__more">{{pll__('По окончании программы вы сможете:', 'Maison')}}</p>
    <ul class="program__list">
      {!! App::printRepeatorField('program-list', 'partials.blocks.programs.program-item') !!}
    </ul>
    <a href="javascript://" class="program__link get-popup" data-popup="more{{get_the_ID()}}">{{pll__('Читать подробнее', 'Maison')}}</a>
    <a href="javascript://" class="program__button button get-popup" data-popup="order{{get_the_ID()}}">
      {{pll__('Записаться', 'Maison')}}
      <span class="button__icon">
        @include('partials.icons.pen')
      </span>
    </a>
  </div>
  @include('partials.blocks.popup.popup-more')
  @include('partials.blocks.popup.popup-form')
</li>
