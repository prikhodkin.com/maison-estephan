<div class="subscribe" >
  <div class="subscribe__input">
    <input autocomplete="off" type="email" name="email" placeholder="{{pll__('Ваш e-mail', 'Maison')}}">
  </div>
</div>
<button type="submit" class="popup__button button">
  {{pll__('Записаться', 'Maison')}}
  <span class="button__icon">
    @include('partials.icons.pen')
  </span>
</button>
