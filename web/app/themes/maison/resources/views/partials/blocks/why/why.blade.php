<section class="why">
  <h2 class="why__title title">{{get_field('why-title')}}</h2>
  <div class="why__place place">
    <p class="place__header">{!!get_field('why-caption')!!}</p>
    <div class="place__content">
      {!! get_field('why-text') !!}
    </div>
  </div>
  <img src="{{get_field('why-img')}}" alt="{{get_field('why-title')}}" class="why__img">
</section>
