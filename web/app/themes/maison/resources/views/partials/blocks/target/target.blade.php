<section class="target">
  <h2 class="visually-hidden">Наша цель</h2>
{{--  <div class="target__top">--}}
{{--    <img src="{{get_field('target-img')}}" class="target__img">--}}
{{--    <p class="target__desc">{{get_field('target-desc')}}</p>--}}
{{--  </div>--}}
  <div class="target__place place">
    <p class="place__header">{{get_field('target-header')}}</p>
    <div class="place__content">
      {!! get_field('target-content') !!}
    </div>
  </div>
  <p class="target__quote quote">{{get_field('target-quote')}}</p>
</section>
