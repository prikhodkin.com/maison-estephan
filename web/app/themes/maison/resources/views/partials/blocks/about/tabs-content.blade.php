<div class="tabs__content  @if($key === 0) tabs__content--active @endif " data-content="{{$key}}">
  <div class="about__area">
    <ul class="about__list">
      @foreach($row['about-item']['list'] as $item)
        <li class="about__item">
          <div class="about__inner">
            <p class="about__pos">{{$item['position']}}</p>
            <p class="about__loc">{{$item['location']}}</p>
          </div>
        </li>
      @endforeach
    </ul>
  </div>
</div>
