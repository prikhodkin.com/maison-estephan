<section class="reviews" id="reviews">
  <h2 class="reviews__title extratitle">{{get_field('reviews-title','option')}}</h2>
  <ul class="reviews__list">
    {!! App::printRepeatorField('reviews-list', 'partials.blocks.reviews.reviews-item', 'option') !!}
  </ul>
  <div class="reviews__controls">
    <button class="reviews__control reviews__control--prev">
      @include('partials.icons.arrow-left')
    </button>
    <div class="reviews__counter">
      <span class="reviews__current">1</span> из <span class="reviews__lenght">5</span>
    </div>
    <button class="reviews__control reviews__control--next">
      @include('partials.icons.arrow-right')
    </button>
  </div>
</section>
