<div class="popup popup--form" data-popup="order{{get_the_ID()}}">
  <div class="popup__wrapper">
    <div class="popup__content">
      <button class="popup__close">
        @include('partials.icons.close')
      </button>
      <h2 class="popup__title title"> {{the_title()}}</h2>
      <p class="popup__desc">{{pll__('Оставьте свой номер телефона, мы свяжемся с вами в ближайшее время и договоримся об удобной дате консультации', 'Maison')}}</p>
      <form class="popup__form js-callback form">
        <input type="hidden" name="action" value="program_action">
        <input type="hidden" name="name" value="{{the_title()}}">
       @include('partials.blocks.popup.popup-form-tpl')
      </form>
      <label class="popup__label">
        <input type="checkbox" checked>
        <span class="popup__checkbox"></span>
        {{pll__('Даю согласие на обработку ', 'Maison')}}
        <a href="{{get_privacy_policy_url()}}">&nbsp;{{pll__(' персональных данных', 'Maison')}}</a>
      </label>
    </div>
  </div>
</div>
