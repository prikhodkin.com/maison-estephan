<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M7 9H5V11H7V9Z" fill="#DD4D39"/>
  <path d="M11 9H9V11H11V9Z" fill="#DD4D39"/>
  <path d="M15 9H13V11H15V9Z" fill="#DD4D39"/>
  <path d="M7 13H5V15H7V13Z" fill="#DD4D39"/>
  <path d="M11 13H9V15H11V13Z" fill="#DD4D39"/>
  <path d="M15 13H13V15H15V13Z" fill="#DD4D39"/>
  <path d="M19 2H17V1C17 0.4 16.6 0 16 0C15.4 0 15 0.4 15 1V2H5V1C5 0.4 4.6 0 4 0C3.4 0 3 0.4 3 1V2H1C0.4 2 0 2.4 0 3V19C0 19.6 0.4 20 1 20H19C19.5 20 20 19.6 20 19V3C20 2.4 19.5 2 19 2ZM18 18H2V6H18V18Z" fill="#DD4D39"/>
</svg>
