<section class="question">
  <div class="question__content">
    <h2 class="question__title title">{!! get_field('question-title', 'option') !!}</h2>
    <ul class="question__list">
      <li class="question__item question__item--email">
        <a href="mailto:{{get_field('email', 'option')}}">{{get_field('email', 'option')}}</a>
      </li>
      <li class="question__item question__item--phone">
        <a href="tel:{{App::NormalizePhone(get_field('phone-ru', 'option'))}}"><span>(RU)</span> {!! get_field('phone-ru', 'option') !!}</a>
      </li>
      <li class="question__item question__item--phone">
        <a href="tel:{{App::NormalizePhone(get_field('phone-fr', 'option'))}}"><span>(FR)</span> {!! get_field('phone-fr', 'option') !!}</a>
      </li>
    </ul>
  </div>
  <div class="question__img">
    <img src="{{get_field('question-img', 'option')}}">
  </div>
</section>
