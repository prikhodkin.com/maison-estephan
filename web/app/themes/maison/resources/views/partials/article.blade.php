<li class="layout__item article">
  <a href="{!! get_permalink() !!}" class="article__img">
    <img src="{{get_field('post-preview')}}" alt="{!! get_the_title() !!}">
  </a>
  <span class="article__date">{{get_the_date('d.m.Y')}}</span>
  <a href="{!! get_permalink() !!}" class="article__header">{!! get_the_title() !!}</a>
  <a href="{!! get_permalink() !!}" class="article__link">{{pll__('Читать статью', 'Maison')}}</a>
</li>
