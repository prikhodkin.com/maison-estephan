<li class="social__item">
  <a href="{{$row['social-item']['social-link']}}">
    <img class="style-svg" src="{{$row['social-item']['social-icon']}}" alt="{{$row['social-item']['social-name']}}">
  </a>
</li>
