<footer class="footer">
  <div class="footer__top">
    <a @if(!is_front_page()) href="{{ home_url('/') }}" @endif class="footer__logo">
      <img src="{{get_field('logo', 'option')}}" alt="{{ get_bloginfo('name', 'display') }}">
    </a>
    {!! App::footerMenu() !!}
    <div class="footer__col">
      <div class="footer__row">
        <p class="footer__header">{{pll__('Подписаться на рассылку:', 'Maison')}}</p>
        @include('partials.subscribe.subscribe', ['class' => 'footer__form'])
      </div>
      <div class="footer__row">
        <p class="footer__header">{{pll__('Следите за нами:', 'Maison')}}</p>
        <ul class="footer__social social">
          {!! App::printRepeatorField('social', 'partials.social.social-item', 'option') !!}
        </ul>
      </div>
    </div>
  </div>
  <div class="footer__bottom">
    <a href="{{get_privacy_policy_url()}}" class="footer__politic">{{pll__('Политика конфиденциальности', 'Maison')}}</a>
    <p class="footer__copyright">{{get_field('copyright', 'option')}}</p>
    <a href="https://inalion.com" class="footer__develop">
      <img src="{{get_template_directory_uri()}}/assets/images/inalion.png" alt="inalion">
      <p>{{pll__('Разработка веб-сайтов', 'Maison')}}</p>
    </a>
  </div>
</footer>
@include('partials.blocks.popup.popup')
