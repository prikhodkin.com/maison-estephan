<div class="breadcrumbs">
    <ul class="breadcrumbs__list" itemscope="" itemtype="https://schema.org/BreadcrumbList">
      @if(function_exists('bcn_display'))
        {{bcn_display()}}
      @endif
    </ul>
</div>
