<li class="other-programs__item" id="{!! str_replace(" ", "-", strtolower(get_the_title())) !!}">
  <h3 class="other-programs__title">{{the_title()}}</h3>
  <div class="other-programs__content">
    {!! get_field('program-content') !!}
  </div>
{{--  <a href="javascript://" class="program__link get-popup" data-popup="more{{get_the_ID()}}">{{pll__('Читать подробнее', 'Maison')}}</a>--}}
  <a href="javascript://" class="program__button button get-popup" data-popup="order{{get_the_ID()}}">
    {{pll__('Записаться', 'Maison')}}
    <span class="button__icon">
      @include('partials.icons.pen')
    </span>
  </a>
  @include('partials.blocks.popup.popup-more')
  @include('partials.blocks.popup.popup-form')
</li>
